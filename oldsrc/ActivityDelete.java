package com.gmail.warr1024.blindpass;


public class ActivityDelete extends ActivitySearchBase
	{
	private final String TAG = ActivityDelete.class.getName();

	private String masterPassword = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		if(BlindPassWorker.getInstance().isRunning())
			searchLayout(savedInstanceState);
		else
			setContentView(R.layout.delete);

		Intent intent = getIntent();
		if(intent == null)
			finish();
		masterPassword = intent.getStringExtra(ActivitySignin.MASTER_PASSWORD);
		if(masterPassword == null)
			finish();
		}

	private void startSearch()
		{
		String serviceName = ((EditText)findViewById(R.id.delete_svcname)).getText().toString();

		BlindPassWorker.getInstance().getListeners().add(new BlindPassWorkerListenerBase()
			{
			@Override
			public Boolean onWorkerCommit(BlindPassSearch search)
				{
				BlindPassWorker.getInstance().getListeners().remove(this);
				try
					{
					BlindPassDb.getInstance().removeData(search.getHashId());
					return true;
					}
				catch(Exception ex)
					{
					Log.e(TAG, ex.getMessage(), ex);
					return false;
					}
				}
			});

		BlindPassSearch search = new BlindPassSearch(masterPassword, serviceName, 0);
		BlindPassWorker.getInstance().start(search);

		searchLayout(null);
		}

	public void doSearch(View v)
		{
		if(!BlindPassWorker.getInstance().isRunning())
			startSearch();
		else
			searchLayout(null);
		}

	@Override
	public String getSuccessText()
		{
		return getString(R.string.delete_success);
		}
	}
