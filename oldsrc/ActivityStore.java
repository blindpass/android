package com.gmail.warr1024.blindpass;


public class ActivityStore extends ActivitySearchBase
	{
	private static final String TAG = ActivitySearchBase.class.getName();
	private static final int SECLV_MIN = 4;
	private static final int SECLV_MAX = 32;

	private String masterPassword = null;

	public int getUISecurityLevel()
		{
		return ((SeekBar)findViewById(R.id.store_secbar)).getProgress() + SECLV_MIN;
		}

	public void onSeekBarSeek()
		{
		int lv = getUISecurityLevel();

		String txt = getString(R.string.store_seclevel, lv);

		double avg = BlindPassBenchmark.getInstance().getAvgHashSpeed();
		if(avg > 0)
			{
			long est = 2;
			for(int i = 0; i < lv; i++, est <<= 1);
			est = (long)((double)est / avg);
			int unit = (est > 1) ? R.string.store_t_mils : R.string.store_t_mil;
			if(est > 86400000)
				{
				est = est / 86400000;
				unit = (est > 1) ? R.string.store_t_days : R.string.store_t_day;
				}
			else if(est > 3600000)
				{
				est = est / 3600000;
				unit = (est > 1) ? R.string.store_t_hrs : R.string.store_t_hr;
				}
			else if(est > 60000)
				{
				est = est / 60000;
				unit = (est > 1) ? R.string.store_t_mins : R.string.store_t_min;
				}
			else if(est > 1000)
				{
				est = est / 1000;
				unit = (est > 1) ? R.string.store_t_secs : R.string.store_t_sec;
				}
			txt += getString(R.string.store_secest, est, getString(unit));
			}

		((TextView)findViewById(R.id.store_seclevel)).setText(txt);
		}

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		if(BlindPassWorker.getInstance().isRunning())
			searchLayout(savedInstanceState);
		else
			setContentView(R.layout.store);

		Intent intent = getIntent();
		if(intent == null)
			finish();
		masterPassword = intent.getStringExtra(ActivitySignin.MASTER_PASSWORD);
		if(masterPassword == null)
			finish();

		final ActivityStore self = this;
		View bar = findViewById(R.id.store_secbar);
		if((bar != null) && (bar instanceof SeekBar))
			{
			((SeekBar)bar).setMax(SECLV_MAX - SECLV_MIN);
			((SeekBar)bar).setOnSeekBarChangeListener(
				new SeekBar.OnSeekBarChangeListener()
					{
					@Override
					public void onStartTrackingTouch(SeekBar bar) { }

					@Override
					public void onStopTrackingTouch(SeekBar bar) { }

					@Override
					public void onProgressChanged(SeekBar bar, int progress, boolean user)
						{
						self.onSeekBarSeek();
						}
					});

			double target = 20000 * BlindPassBenchmark.getInstance().getAvgHashSpeed();
			int lv = 0;
			for(double m = 2; m < target; m *= 2, lv++);
			if(lv > SECLV_MAX)
				((SeekBar)bar).setProgress(SECLV_MAX);
			else if(lv > SECLV_MIN)
				((SeekBar)bar).setProgress(lv - SECLV_MIN);

			onSeekBarSeek();
			}
		}

	private void startSearch()
		{
		String serviceName = ((EditText)findViewById(R.id.store_svcname)).getText().toString();

		String svcPass = ((EditText)findViewById(R.id.store_svcpass)).getText().toString();
		if((svcPass == null) || (svcPass.length() < 4))
			{
			alert(getString(R.string.store_err_pwshort, 4));
			return;
			}
		if(svcPass.length() > 32)
			{
			alert(getString(R.string.store_err_pwlong, 32));
			return;
			}
		byte[] whitened = null;
		try { whitened = BlindPassCrypto.getInstance().whitenEncode(svcPass); }
		catch(Exception ex)
			{
			alert(getString(R.string.store_err_pwenc, ex.getMessage()));
			return;
			}
		final byte[] finalWhitened = whitened;

		int securityLevel = getUISecurityLevel();

		BlindPassWorker.getInstance().getListeners().add(new BlindPassWorkerListenerBase()
			{
			@Override
			public Boolean onWorkerCommit(BlindPassSearch search)
				{
				BlindPassWorker.getInstance().getListeners().remove(this);
				try
					{
					BlindPassDb.getInstance().setData(search.getHashId(), BlindPassCrypto
						.getInstance().encrypt(finalWhitened, search.getHashKey()), 2);
					return true;
					}
				catch(Exception ex)
					{
					Log.e(TAG, ex.getMessage(), ex);
					return false;
					}
				}
			});

		BlindPassSearch search = new BlindPassSearch(masterPassword, serviceName, securityLevel);
		BlindPassWorker.getInstance().start(search);

		searchLayout(null);
		}

	public void doSearch(View v)
		{
		if(!BlindPassWorker.getInstance().isRunning())
			startSearch();
		else
			searchLayout(null);
		}

	@Override
	public String getSuccessText()
		{
		return getString(R.string.store_success);
		}
	}
