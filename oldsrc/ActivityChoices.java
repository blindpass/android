package com.gmail.warr1024.blindpass;


public class ActivityChoices extends ActivityBase
	{
	private String MasterPassword = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choices);

		Intent intent = getIntent();
		if(intent == null)
			finish();
		MasterPassword = intent.getStringExtra(ActivitySignin.MASTER_PASSWORD);
		if(MasterPassword == null)
			finish();
		}

	private void StartAct(Class act)
		{
		Intent intent = new Intent(this, act);
		intent.putExtra(ActivitySignin.MASTER_PASSWORD, MasterPassword);
		startActivity(intent);
		}

	public void doGet(View view)
		{
		StartAct(ActivityRetrieve.class);
		}

	public void doSet(View view)
		{
		StartAct(ActivityStore.class);
		}

	public void doDel(View view)
		{
		StartAct(ActivityDelete.class);
		}
	}
