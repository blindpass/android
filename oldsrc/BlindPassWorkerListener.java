package com.gmail.warr1024.blindpass;

public interface BlindPassWorkerListener
	{
	void onWorkerProgress(BlindPassSearch search);
	Boolean onWorkerCommit(BlindPassSearch search);
	void onWorkerFinish(BlindPassSearch search);
	void onWorkerError(BlindPassSearch search);
	}
