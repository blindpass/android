package com.gmail.warr1024.blindpass;


public class ActivityRetrieve extends ActivitySearchBase
	{
	private final String TAG = ActivityRetrieve.class.getName();

	private String masterPassword = null;

	private Boolean showResult()
		{
		BlindPassSearch search = BlindPassWorker.getInstance().getLastResult();
		if(search == null)
			return false;
 
		setContentView(R.layout.blank);
		try
			{
			String pw = BlindPassCrypto.getInstance().whitenDecode(
				BlindPassCrypto.getInstance().decrypt(
				BlindPassDb.getInstance().getData(search.getHashId()),
				search.getHashKey()));
			alert(getString(R.string.retrieve_result, pw), new Runnable()
				{
				@Override
				public void run()
					{
					BlindPassWorker.getInstance().clearLastResult();
					finish();
					}
				});
			}
		catch(Exception ex)
			{
			Log.e(TAG, ex.getMessage(), ex);
			BlindPassWorker.getInstance().clearLastResult();
			alert(ex, new Runnable()
				{
				@Override
				public void run()
					{
					finish();
					}
				});
			}
		return true;
		}

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		if(BlindPassWorker.getInstance().isRunning())
			{
			if(!showResult())
				searchLayout(savedInstanceState);
			}
		else
			setContentView(R.layout.retrieve);

		Intent intent = getIntent();
		if(intent == null)
			finish();
		masterPassword = intent.getStringExtra(ActivitySignin.MASTER_PASSWORD);
		if(masterPassword == null)
			finish();
		}

	private void startSearch()
		{
		String serviceName = ((EditText)findViewById(R.id.retrieve_svcname)).getText().toString();

		BlindPassSearch search = new BlindPassSearch(masterPassword, serviceName, 0);
		BlindPassWorker.getInstance().start(search);

		searchLayout(null);
		}

	@Override
	public void onWorkerFinish(BlindPassSearch search)
		{
		showResult();
		}

	public void doSearch(View v)
		{
		if(!BlindPassWorker.getInstance().isRunning())
			startSearch();
		else
			searchLayout(null);
		}
	}
