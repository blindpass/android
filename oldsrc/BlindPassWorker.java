package com.gmail.warr1024.blindpass;


public class BlindPassWorker
	{
	private static BlindPassWorker instance = null;

	public static BlindPassWorker getInstance()
		{
		if(instance == null)
			instance = new BlindPassWorker();
		return instance;
		}

	private AsyncTask<Void, Void, Void> worker = null;
	private Set<BlindPassWorkerListener> listeners
		= Collections.synchronizedSet(new HashSet<BlindPassWorkerListener>());
	private BlindPassSearch lastResult = null;

	public Set<BlindPassWorkerListener> getListeners()
		{
		return listeners;
		}
	public synchronized BlindPassSearch getLastResult() { return lastResult; }
	public synchronized void clearLastResult() { lastResult = null; }

	private BlindPassWorker() { }

	public void start(final BlindPassSearch search)
		{
		if(worker != null)
			return;
		final BlindPassWorker self = this;
		worker = new AsyncTask<Void, Void, Void>()
			{
			private Exception error = null;

			@Override
			protected void onPreExecute() { }

			@Override
			protected Void doInBackground(Void... params)
				{
				Timer tick = new Timer();
				try
					{
					tick.schedule(new TimerTask()
						{
						@Override
						public void run()
							{
							publishProgress();
							}
						}, 250, 250);
					while(!isCancelled() && search.doIteration());
					return null;
					}
				finally
					{
					tick.cancel();
					tick.purge();
					}
				}

			@Override
			protected void onProgressUpdate(Void... progress)
				{
				Set<BlindPassWorkerListener> listeners
					= new HashSet<BlindPassWorkerListener>(self.getListeners());
				synchronized(listeners)
					{
					for(BlindPassWorkerListener l : listeners)
						l.onWorkerProgress(search);
					}
				}

			@Override
			protected void onPostExecute(Void result)
				{
				if(isCancelled())
					return;
				Set<BlindPassWorkerListener> listeners
					= new HashSet<BlindPassWorkerListener>(self.getListeners());
				synchronized(listeners)
					{
					Boolean ok = true;
					for(BlindPassWorkerListener l : listeners)
						ok &= l.onWorkerCommit(search);
					if(ok)
						synchronized(self)
							{
							self.lastResult = search;
							}
					for(BlindPassWorkerListener l : listeners)
						if(ok)
							l.onWorkerFinish(search);
						else
							l.onWorkerError(search);
					}
				}
			};
		worker.execute();
		}

	public void cancel()
		{
		if(worker != null)
			{
			worker.cancel(false);
			worker = null;
			}
		clearLastResult();
		}

	public Boolean isRunning()
		{
		return (worker != null) && (worker.getStatus() == AsyncTask.Status.RUNNING);
		}
	}
