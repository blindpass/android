package com.gmail.warr1024.blindpass;

public class BlindPassWorkerListenerBase implements BlindPassWorkerListener
	{
	public void onWorkerProgress(BlindPassSearch search) { }
	public Boolean onWorkerCommit(BlindPassSearch search) { return true; }
	public void onWorkerFinish(BlindPassSearch search) { }
	public void onWorkerError(BlindPassSearch search) { }
	}
