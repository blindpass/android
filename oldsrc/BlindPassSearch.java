package com.gmail.warr1024.blindpass;


public class BlindPassSearch
	{
	private static final String TAG = BlindPassSearch.class.getName();

	private long depthValue = 0;
	private byte[] masterPassBytes = null;
	private byte[] serviceNameBytes = null;
	private HashSet<ByteBuffer> allKeys;

	public BlindPassSearch(String masterPass, String serviceName, int securityLevel)
		{
		masterPassBytes = masterPass.getBytes();
		serviceNameBytes = serviceName.getBytes();

		if(securityLevel > 0)
			{
			depthValue = 1;
			for(int i = 0; i < securityLevel; i++)
				depthValue <<= 1;
			}

		allKeys = BlindPassDb.getInstance().getAllKeys();

		hashId = new byte[256 / 8];
		hashKey = new byte[256 / 8];
		}

	private volatile long iteration = 0;
	private volatile byte[] hashId = null;
	private volatile byte[] hashKey = null;

	public long getIteration() { return iteration; }
	public byte[] getHashId() { return hashId; }
	public byte[] getHashKey() { return hashKey; }

	public Boolean doIteration()
		{
		try
			{
			if((depthValue > 0) && (iteration >= depthValue)
				&& (BlindPassCrypto.getInstance().lasVegasRandom(1, depthValue) == 1))
				return false;

			hashKey = hashId;
			hashId = BlindPassCrypto.getInstance().hash(hashId, masterPassBytes, serviceNameBytes);
			}
		catch(Exception ex)
			{
			Log.e(TAG, ex.getMessage(), ex);
			return false;
			}

		iteration++;
		return !allKeys.contains(ByteBuffer.wrap(hashId));
		}
	}
