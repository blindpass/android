package com.gmail.warr1024.blindpass;

import android.os.AsyncTask;
import android.text.format.Time;
import java.lang.Runnable;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EngineSearch
	{
	public static final long MIN_DEPTH = 64;

	private static EngineSearch instance = null;

	public static EngineSearch getInstance()
		{
		if(instance == null)
			instance = new EngineSearch();
		return instance;
		}

	private byte[] masterPassBytes = null;
	private byte[] serviceNameBytes = null;
	private HashSet<ByteBuffer> allKeys;

	private long depth = 0;
	private byte[] hashId = null;
	private byte[] hashKey = null;

	private AsyncTask<Void, Void, Void> worker = null;
	private Time startTime = null;
	private volatile long iteration = 0;
	private List<SearchResult> found = null;
	private Set<Runnable> listeners = null;

	private EngineSearch()
		{
		found = new ArrayList<SearchResult>(32);
		listeners = Collections.synchronizedSet(new HashSet<Runnable>());
		}
		
	public long getIteration()
		{
		return iteration;
		}

	public Time getStartTime()
		{
		synchronized(startTime)
			{
			return new Time(startTime);
			}
		}

	public SearchResult[] getResults()
		{
		synchronized(found)
			{
			return found.toArray(new SearchResult[0]);
			}
		}

	public Set<Runnable> getListeners()
		{
		return listeners;
		}

	public Boolean isRunning()
		{
		return (worker != null) && (worker.getStatus() == AsyncTask.Status.RUNNING);
		}

	public void start(String serviceName)
		{
		if(isRunning())
			return;

		iteration = 0;
		found.clear();

		masterPassBytes = AppApplication.getInstance().masterPassword.getBytes();
		serviceNameBytes = serviceName.getBytes();

		depth = MIN_DEPTH;
		hashId = new byte[256 / 8];
		hashKey = new byte[256 / 8];

		startTime = new Time();
		startTime.setToNow();

		final EngineSearch self = this;
		worker = new AsyncTask<Void, Void, Void>()
			{
			@Override
			protected void onPreExecute() { }

			private void addResult(boolean exists, byte[] hashId, byte[] hashKey)
				{
				byte[] copyId = new byte[hashId.length];
				System.arraycopy(hashId, 0, copyId, 0, hashId.length);
				byte[] copyKey = new byte[hashKey.length];
				System.arraycopy(hashKey, 0, copyKey, 0, hashKey.length);
				Time runTime = new Time();
				runTime.setToNow();
				synchronized(found)
					{
					found.add(new SearchResult(iteration,
						runTime.toMillis(false), exists, copyId, copyKey));
					}
				publishProgress();
				}

			@Override
			protected Void doInBackground(Void... params)
				{
				try
					{
					MessageDigest sha256 = MessageDigest.getInstance("SHA256");
					hashId = new byte[sha256.getDigestLength()];
					hashKey = new byte[hashId.length];

					byte[] inbuff = new byte[12 + sha256.getDigestLength()
						+ masterPassBytes.length + serviceNameBytes.length];
					int pos = 0;
					EngineCrypto.getInstance().packUInt32(inbuff, pos, hashId.length);
					pos += 4;
					pos += hashId.length;
					EngineCrypto.getInstance().packUInt32(inbuff, pos, masterPassBytes.length);
					pos += 4;
					System.arraycopy(masterPassBytes, 0, inbuff, pos, masterPassBytes.length);
					pos += masterPassBytes.length;
					EngineCrypto.getInstance().packUInt32(inbuff, pos, serviceNameBytes.length);
					System.arraycopy(serviceNameBytes, 0, inbuff, pos, serviceNameBytes.length);

					allKeys = EngineDB.getInstance().getKeys();
					ByteBuffer hashIdBuff = ByteBuffer.wrap(hashId);

					synchronized(startTime)
						{
						startTime.setToNow();
						}

					while(!isCancelled())
						{
						System.arraycopy(hashId, 0, hashKey, 0, hashId.length);
						System.arraycopy(hashId, 0, inbuff, 4, hashId.length);
						sha256.update(inbuff);
						sha256.digest(hashId, 0, hashId.length);
						sha256.reset();

						iteration++;

						if(allKeys.contains(hashIdBuff))
							addResult(true, hashId, hashKey);
						else if((depth > 0) && (iteration >= depth)
							&& (EngineCrypto.getInstance().lasVegasRandom(1, depth) == 1))
							{
							addResult(false, hashId, hashKey);
							depth <<= 1;
							}
						}
					}
				catch(Exception ex)
					{
					throw new RuntimeException(ex);
					}
				return null;
				}

			@Override
			protected void onProgressUpdate(Void... progress)
				{
				for(Runnable r : self.getListeners().toArray(new Runnable[0]))
					r.run();
				}

			@Override
			protected void onPostExecute(Void result)
				{
				for(Runnable r : self.getListeners().toArray(new Runnable[0]))
					r.run();
				}
			};
		worker.execute();
		}

	public void stop()
		{
		if(isRunning())
			worker.cancel(false);
		worker = null;
		}
	}
