package com.gmail.warr1024.blindpass;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ActivitySearchDelete extends ActivitySearchBase
	{
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.delete);
		}

	public void doDelete(View v)
		{
		try
			{
			EngineSearch.getInstance().stop();
			for(SearchResult r : EngineSearch.getInstance().getResults())
				if(r.getExists())
					EngineDB.getInstance().remove(r.getId());
			AppApplication.toast(R.string.delete_done);
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}

		finish();
		}
	}
