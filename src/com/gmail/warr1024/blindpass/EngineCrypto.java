package com.gmail.warr1024.blindpass;

import java.security.MessageDigest;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EngineCrypto
	{
	private static EngineCrypto instance = null;

	public static EngineCrypto getInstance() throws Exception
		{
		if(instance == null)
			instance = new EngineCrypto();
		return instance;
		}

	private long[] getLongArray(int id)
		{
		AppApplication ctx = AppApplication.getInstance();
		String[] strs = ctx.getResources().getStringArray(id);
		long[] parsed = new long[strs.length];
		for(int i = 0; i < strs.length; i++)
			parsed[i] = Long.parseLong(strs[i].toString());
		return parsed;
		}

	private final long[] freqLen;
	private final long[] freqChar;
	private final SecureRandom rand;
	private final MessageDigest sha256;
	private final Cipher aes;
	private final byte[] iv;

	public EngineCrypto() throws Exception
		{
		freqLen = getLongArray(R.array.freq_len);
		freqChar = getLongArray(R.array.freq_char);
		rand = new SecureRandom();
		sha256 = MessageDigest.getInstance("SHA256");
		aes = Cipher.getInstance("AES/CBC/NoPadding");
		iv = new byte[aes.getBlockSize()];
		}

	public long lasVegasRandom(long min, long max)
		{
		if(min == max)
			return min;
		if(min > max)
			{
			long tmp = max;
			max = min;
			min = tmp;
			}
		long range = max - min;
		int bits = 1;
		long mask = 1;
		for(; mask < range; mask = (mask << 1) + 1, bits++);
		byte[] buff = new byte[(bits + 7) >> 3];
		while(true)
			{
			rand.nextBytes(buff);
			long value = 0;
			for(byte b : buff)
				value = (value << 8) + b;
			value = value & mask;
			if(value <= range)
				return min + value;
			}
		}

	public void packUInt32(byte[] buff, int pos, long topack)
		{
		buff[pos + 0] = (byte)((topack >> 24) & (long)255);
		buff[pos + 1] = (byte)((topack >> 16) & (long)255);
		buff[pos + 2] = (byte)((topack >>  8) & (long)255);
		buff[pos + 3] = (byte)((topack >>  0) & (long)255);
		}

	public long unpackUInt32(byte[] buff, int pos)
		{
		return (((long)buff[pos + 0] & (long)255) << 24)
			| (((long)buff[pos + 1] & (long)255) << 16)
			| (((long)buff[pos + 2] & (long)255) << 8)
			| (((long)buff[pos + 3] & (long)255) << 0);
		}

	public byte[] whitenEncode(String pass)
		{
		int lenidx = pass.length() - 4;
		int lenmax = freqLen.length + 4;
		if((lenidx < 0) || (lenidx >= freqLen.length))
			return null;

		byte[] buff = new byte[EngineDB.VALUE_LENGTH];
		rand.nextBytes(buff);

		long min = 0;
		if(lenidx > 0)
			min = freqLen[lenidx - 1];
		long max = freqLen[lenidx] - 1;
		packUInt32(buff, 0, lasVegasRandom(min, max));

		char[] chars = pass.toCharArray();
		for(int i = 0; i < pass.length(); i++)
			{
			int charidx = (int)chars[i] - 32;
			if((charidx < 0) || (charidx >= freqChar.length))
				return null;

			min = 0;
			if(charidx > 0)
				min = freqChar[charidx - 1];
			max = freqChar[charidx] - 1;
			packUInt32(buff, (i + 1) * 4, lasVegasRandom(min, max));
			}

		return buff;
		}

	public String whitenDecode(byte[] buff)
		{
		if(EngineDB.VALUE_LENGTH != buff.length)
			return null;

		long codelen = unpackUInt32(buff, 0);
		int len = 0;
		for(; freqLen[len] < codelen; len++);
		len += 4;

		char[] chars = new char[len];
		for(int i = 0; i < len; i++)
			{
			long codechar = unpackUInt32(buff, (i + 1) * 4);
			int code = 0;
			for(; freqChar[code] < codechar; code++);
			chars[i] = (char)(code + 32);
			}

		return new String(chars);
		}

	public byte[] encrypt(byte[] data, byte[] key) throws Exception
		{
		aes.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"),
			new IvParameterSpec(iv));
		return aes.doFinal(data);
		}

	public byte[] decrypt(byte[] data, byte[] key) throws Exception
		{
		aes.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"),
			new IvParameterSpec(iv));
		return aes.doFinal(data);
		}
	}
