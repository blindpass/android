package com.gmail.warr1024.blindpass;

public class SearchResult
	{
	private long iteration;
	private long searchTime;
	private boolean exists;
	private byte[] id;
	private byte[] key;

	public long getIteration() { return iteration; }
	public long getSearchTime() { return searchTime; }
	public boolean getExists() { return exists; }
	public byte[] getId() { return id; }
	public byte[] getKey() { return key; }

	public SearchResult(long newIteration, long newTime,
		boolean newExists, byte[] newId, byte[] newKey)
		{
		iteration = newIteration;
		searchTime = newTime;
		exists = newExists;
		id = newId;
		key = newKey;
		}
	}
