package com.gmail.warr1024.blindpass;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ActivitySignin extends ActivityBase
	{
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		if(!isTaskRoot())
			finish();
		setContentView(R.layout.signin);
		}

	public void doSignIn(View view)
		{
		String a = ((EditText)findViewById(R.id.signin_masterpw1)).getText().toString();
		String b = ((EditText)findViewById(R.id.signin_masterpw2)).getText().toString();
		if((a == null) || (a.length() < 1)
			|| (b == null) || (b.length() < 1)
			|| !a.equals(b))
			{
			alert(R.string.signin_err_pw);
			return;
			}

		((EditText)findViewById(R.id.signin_masterpw1)).setText("");
		((EditText)findViewById(R.id.signin_masterpw2)).setText("");
		AppApplication.getInstance().masterPassword = a;

		startActivity(ActivityService.class);
		}
	}
