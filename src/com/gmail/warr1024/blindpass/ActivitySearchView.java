package com.gmail.warr1024.blindpass;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

public class ActivitySearchView extends ActivitySearchBase
	{
	ArrayList<String> items = null;
	ArrayAdapter<String> adapter = null;

	@Override
	public void onSearchUpdate()
		{
		super.onSearchUpdate();

		boolean dirty = false;
		int f = 0;
		for(SearchResult r : EngineSearch.getInstance().getResults())
			if(r.getExists())
				{
				f++;
				if(f > items.size())
					try
						{
						items.add(EngineCrypto.getInstance().whitenDecode(
							EngineCrypto.getInstance().decrypt(
							EngineDB.getInstance().get(r.getId()), r.getKey())));
						dirty = true;
						}
					catch(Exception ex)
						{
						throw new RuntimeException(ex);
						}
				}
		if(dirty)
			adapter.notifyDataSetChanged();
		}

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view);

		items = new ArrayList<String>(32);
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
		((ListView)findViewById(R.id.view_results)).setAdapter(adapter);
		}
	}
