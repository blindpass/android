package com.gmail.warr1024.blindpass;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ActivitySearchActions extends ActivitySearchBase
	{
	private void layoutPending()
		{
		boolean pend = !EngineSearch.getInstance().getListeners().isEmpty();
		findViewById(R.id.actions_layout_opts).setVisibility(pend ? View.GONE : View.VISIBLE);
		findViewById(R.id.actions_layout_pend).setVisibility(pend ? View.VISIBLE: View.GONE);
		}

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actions);
		}

	@Override
	public void onResume()
		{
		super.onResume();
		layoutPending();
		}

	public void doGet(View v)
		{
		startActivity(ActivitySearchView.class);
		}

	public void doSet(View v)
		{
		startActivity(ActivitySearchStore.class);
		}

	public void doDel(View v)
		{
		startActivity(ActivitySearchDelete.class);
		}

	public void doCancel(View v)
		{
		EngineSearch.getInstance().getListeners().clear();
		AppApplication.toast(R.string.search_pend_cancel);
		layoutPending();
		}
	}
