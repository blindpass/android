package com.gmail.warr1024.blindpass;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class ActivitySearchStore extends ActivitySearchBase
	{
	private static final int SECLV_MIN = 1;
	private static final int SECLV_MAX = 32;

	private long currentDepth = 0;
	private int currentSecurityLevel = 0;
	private long currentSearchSpeed = 0;
	
	public int getUISecurityLevel()
		{
		return ((SeekBar)findViewById(R.id.store_secbar)).getProgress() + SECLV_MIN;
		}
	public void setUISecurityLevel(int newlv)
		{
		((SeekBar)findViewById(R.id.store_secbar)).setProgress(newlv - SECLV_MIN);
		}

	public long estimateDepth(int lv)
		{
		if(lv <= currentSecurityLevel)
			{
			int l = 0;
			for(SearchResult r : EngineSearch.getInstance().getResults())
				if(!r.getExists())
					{
					l++;
					if(l >= lv)
						return r.getIteration();
					}
			}
		long[] ests = new long[lv + 1];
		long exp = EngineSearch.MIN_DEPTH;
		for(int i = 0; i <= lv; i++)
			{
			ests[i] = exp;
			exp <<= 1;
			}
		long est = ests[currentSecurityLevel];
		if(currentDepth > est)
			est = currentDepth;
		est += ests[lv] - ests[currentSecurityLevel];
		return est;
		}

	public String formatEstimate(long est)
		{
		if(est <= 0)
			return getString(R.string.store_t_zero);
		int unit = (est > 1) ? R.string.store_t_secs : R.string.store_t_sec;
		if(est > 86400)
			{
			est = est / 86400;
			unit = (est > 1) ? R.string.store_t_days : R.string.store_t_day;
			}
		else if(est > 3600)
			{
			est = est / 3600;
			unit = (est > 1) ? R.string.store_t_hrs : R.string.store_t_hr;
			}
		else if(est > 60)
			{
			est = est / 60;
			unit = (est > 1) ? R.string.store_t_mins : R.string.store_t_min;
			}
		return getString(R.string.store_t_format, est, getString(unit));
		}

	public void securityLevelDisplay()
		{
		int lv = getUISecurityLevel();
		((TextView)findViewById(R.id.store_seclevel)).setText(
				getString(R.string.store_seclv_lv, lv));

		if(currentSearchSpeed > 0)
			{
			long est = estimateDepth(lv);
			long rem = est - currentDepth;

			est /= currentSearchSpeed;
			rem /= currentSearchSpeed;

			((TextView)findViewById(R.id.store_est_read)).setText(formatEstimate(est));
			((TextView)findViewById(R.id.store_est_write)).setText(formatEstimate(rem));
			}
		}

	@Override
	public void onSearchStats(long iter, long rate, int found, int lv)
		{
		super.onSearchStats(iter, rate, found, lv);

		if((lv >= SECLV_MIN) && (getUISecurityLevel() == currentSecurityLevel))
			setUISecurityLevel(lv);

		currentDepth = iter;
		currentSecurityLevel = lv;
		currentSearchSpeed = rate;

		securityLevelDisplay();
		}
		
	public void onSeekBarSeek()
		{
		securityLevelDisplay();
		}

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.store);

		final ActivitySearchStore self = this;
		View bar = findViewById(R.id.store_secbar);
		if((bar != null) && (bar instanceof SeekBar))
			{
			((SeekBar)bar).setMax(SECLV_MAX - SECLV_MIN);
			((SeekBar)bar).setOnSeekBarChangeListener(
				new SeekBar.OnSeekBarChangeListener()
					{
					@Override
					public void onStartTrackingTouch(SeekBar bar) { }

					@Override
					public void onStopTrackingTouch(SeekBar bar) { }

					@Override
					public void onProgressChanged(SeekBar bar, int progress, boolean user)
						{
						self.onSeekBarSeek();
						}
					});
			setUISecurityLevel(SECLV_MIN);
			currentSecurityLevel = SECLV_MIN;
			onSeekBarSeek();
			}
		}

	public void doStore(View v)
		{
		String svcPass = ((EditText)findViewById(R.id.store_svcpass)).getText().toString();
		if((svcPass == null) || (svcPass.length() < 4))
			{
			alert(getString(R.string.store_err_pwshort, 4));
			return;
			}
		if(svcPass.length() > 32)
			{
			alert(getString(R.string.store_err_pwlong, 32));
			return;
			}
		byte[] whitened = null;
		try { whitened = EngineCrypto.getInstance().whitenEncode(svcPass); }
		catch(Exception ex)
			{
			alert(getString(R.string.store_err_pwenc, ex.getMessage()));
			return;
			}
		final byte[] finalWhitened = whitened;

		final int securityLevel = getUISecurityLevel();

		Runnable toDo = new Runnable()
			{
			private Boolean done = false;

			@Override
			public void run()
				{
				if(done)
					return;

				SearchResult found = null;
				int lv = 0;
				SearchResult[] allResults = EngineSearch.getInstance().getResults();
				for(SearchResult r : allResults)
					if(!r.getExists())
						{
						lv++;
						if(lv >= securityLevel)
							{
							found = r;
							break;
							}
						}
				if(found == null)
					return;

				try
					{
					EngineSearch.getInstance().stop();
					EngineDB.getInstance().put(found.getId(),
						EngineCrypto.getInstance().encrypt(
						finalWhitened, found.getKey()));
					for(SearchResult r : allResults)
						if(r.getExists())
							EngineDB.getInstance().remove(r.getId());
					AppApplication.toast(R.string.store_done);
					}
				catch(Exception ex)
					{
					throw new RuntimeException(ex);
					}

				done = true;
				EngineSearch.getInstance().getListeners().remove(this);
				}
			};
		EngineSearch.getInstance().getListeners().add(toDo);
		toDo.run();

		finish();
		}
	}
