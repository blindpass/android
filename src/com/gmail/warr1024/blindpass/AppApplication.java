package com.gmail.warr1024.blindpass;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;
import java.io.StringWriter;
import java.lang.Thread;

public class AppApplication extends Application
	{
	private static AppApplication instance = null;

	public static AppApplication getInstance()
		{
		return instance;
		}

	public static void toast(String msg)
		{
		Toast.makeText(getInstance(), msg, Toast.LENGTH_SHORT).show();
		}
	public static void toast(int msgId)
		{
		toast(getInstance().getString(msgId));
		}

	public static void log(Throwable ex)
		{
		StringWriter sw = new StringWriter();

		while(ex != null)
			{
			sw.write("" + ex);
			sw.write("\n");

			for(StackTraceElement trace : ex.getStackTrace())
				{
				sw.write("\tat " + trace);
				sw.write("\n");
				}

			ex = ex.getCause();
			if(ex != null)
				sw.write("Caused by:\n");
       			}

		Log.e(getInstance().getResources().getString(
			R.string.app_name), sw.toString());
		}

	public String masterPassword = null;

	public AppApplication()
		{
		instance = this;

		final Thread.UncaughtExceptionHandler oldUIHandler
			= Thread.currentThread().getUncaughtExceptionHandler();
		Thread.currentThread().setUncaughtExceptionHandler(
			new Thread.UncaughtExceptionHandler()
				{
				@Override
				public void uncaughtException(Thread t, Throwable ex)
					{
					log(ex);
					oldUIHandler.uncaughtException(t, ex);
					}
				});

		final Thread.UncaughtExceptionHandler oldDefaultHandler
			= Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(
			new Thread.UncaughtExceptionHandler()
				{
				@Override
				public void uncaughtException(Thread t, Throwable ex)
					{
					log(ex);
					oldDefaultHandler.uncaughtException(t, ex);
					}
				});
		}
	}
