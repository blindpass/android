package com.gmail.warr1024.blindpass;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ActivityService extends ActivityBase
	{
	private AlertDialog cxlDialog = null;

	protected void setCxlDialog(AlertDialog dlg)
		{
		if((cxlDialog != null) && (cxlDialog != dlg))
			cxlDialog.dismiss();
		cxlDialog = dlg;
		if(dlg != null)
			dlg.show();
		findViewById(R.id.service_layout_2).setVisibility(
			(dlg == null) ? View.VISIBLE : View.GONE);
		}

	protected void searchCxlDialog()
		{
		if(!EngineSearch.getInstance().isRunning())
			return;

		if(cxlDialog != null)
			return;

		setCxlDialog( new AlertDialog.Builder(this)
			.setIcon(R.drawable.ic_warning)
			.setTitle(R.string.search_cxl_title)
			.setMessage(R.string.search_cxl_message)
			.setPositiveButton(R.string.search_cxl_pos, new DialogInterface.OnClickListener()
				{
				public void onClick(DialogInterface dialog, int id)
					{
					EngineSearch.getInstance().stop();
					AppApplication.toast(R.string.search_cancel);
					setCxlDialog(null);
					}
				})
			.setNegativeButton(R.string.search_cxl_neg, new DialogInterface.OnClickListener()
				{
				public void onClick(DialogInterface dialog, int id)
					{
					dialog.cancel();
					}
				})
			.setOnCancelListener(new DialogInterface.OnCancelListener()
				{
				public void onCancel(DialogInterface dialog)
					{
					if(EngineSearch.getInstance().isRunning())
						{
						cxlDialog = null;
						startActivity(ActivitySearchActions.class);
						dialog.dismiss();
						return;
						}
					setCxlDialog(null);
					}
				})
			.create());
		}

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.service);
		searchCxlDialog();
		}

	@Override
	public void onResume()
		{
		super.onResume();
		searchCxlDialog();
		}

	@Override
	public void onStop()
		{
		if(isFinishing())
			AppApplication.getInstance().masterPassword = null;
		super.onStop();
		}

	public void doSearch(View v)
		{
		EditText svcName = (EditText)findViewById(R.id.service_svcname);
		EngineSearch.getInstance().start(svcName.getText().toString());
		svcName.setText("");

		startActivity(ActivitySearchActions.class);
		}
	}
