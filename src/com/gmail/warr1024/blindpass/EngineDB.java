package com.gmail.warr1024.blindpass;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Base64;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.HashSet;

public class EngineDB extends SQLiteOpenHelper
	{
	public static final String KVP_TABLE = "PassData";
	public static final String KVP_EXT = ".sqlite";
	public static final String KVP_KEY = "PassKey";
	public static final String KVP_VALUE = "PassValue";
	public static final String KVP_INDEX = "UX_PassData_Key";

	// Database key size: number of bytes in a SHA256.
	public static final int KEY_LENGTH = 256 / 8;

	// Database entry size: 32x int32 for data, plus 1x int32 for length,
	// rounded up to nearest 128-bit AES block size.
	public static final int VALUE_LENGTH = ((32 + 1) * 4 + (128 / 8 - 1)) / (128 / 8) * (128 / 8);
	
	private static EngineDB instance = null;

	public static EngineDB getInstance()
		{
		if(instance == null)
			instance = new EngineDB(AppApplication.getInstance());
		return instance;
		}

	private EngineDB(Context ctx)
		{
		super(ctx, KVP_TABLE + KVP_EXT, null, 1);
		}

	private void remove(byte[] key, SQLiteDatabase db)
		{
		String s = Base64.encodeToString(key, Base64.DEFAULT);
		db.delete(KVP_TABLE, KVP_KEY + " = ?", new String[] { s });
		}

	public void remove(byte[] key)
		{
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();
		try
			{
			remove(key, db);
			db.setTransactionSuccessful();
			}
		finally
			{
			db.endTransaction();
			db.close();
			}
		}

	private void put(byte[] key, byte[] value, SQLiteDatabase db)
		{
		ContentValues cols = new ContentValues();
		cols.put(KVP_KEY, Base64.encodeToString(key, Base64.DEFAULT));
		cols.put(KVP_VALUE, Base64.encodeToString(value, Base64.DEFAULT));
		db.insertOrThrow(KVP_TABLE, null, cols);
		}

	private void chaff(int scale, SQLiteDatabase db) throws Exception
		{
		SecureRandom rand = new SecureRandom();
		byte[] key = new byte[KEY_LENGTH];
		byte[] value = new byte[VALUE_LENGTH];
		while(EngineCrypto.getInstance().lasVegasRandom(1, scale) != 1)
			{
			rand.nextBytes(key);
			rand.nextBytes(value);
			put(key, value, db);
			}
		}

	private void put(byte[] key, byte[] value, int chaffScale)
		{
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();
		try
			{
			if(chaffScale > 0)
				try { chaff(chaffScale, db); }
				catch(Exception ex)
					{
					throw new RuntimeException(ex);
					}
			remove(key, db);
			put(key, value, db);
			if(chaffScale > 0)
				try { chaff(chaffScale, db); }
				catch(Exception ex)
					{
					throw new RuntimeException(ex);
					}
			db.setTransactionSuccessful();
			}
		finally
			{
			db.endTransaction();
			db.close();
			}
		}

	public void put(byte[] key, byte[] value)
		{
		put(key, value, 0);
		}

	private byte[] get(byte[] key, SQLiteDatabase db)
		{
		String k = Base64.encodeToString(key, Base64.DEFAULT);
		Cursor cur = db.query(KVP_TABLE, new String[] { KVP_VALUE },
			KVP_KEY + " = ?", new String[] { k },
			null, null, null);
		try
			{
			if(!cur.moveToFirst())
				return null;
			String s = cur.getString(cur.getColumnIndexOrThrow(KVP_VALUE));
			return Base64.decode(s, Base64.DEFAULT);
			}
		finally
			{
			cur.close();
			}
		}

	public byte[] get(byte[] key)
		{
		SQLiteDatabase db = getReadableDatabase();
		try
			{
			return get(key, db);
			}
		finally
			{
			db.close();
			}
		}

	public HashSet<ByteBuffer> getKeys()
		{
		HashSet<ByteBuffer> Set = new HashSet<ByteBuffer>();
		SQLiteDatabase db = getReadableDatabase();
		try
			{
			Cursor raw = db.query(KVP_TABLE, new String[] { KVP_KEY },
				null, null, null, null, null);
			try
				{
				if(!raw.moveToFirst())
					return Set;
				do
					{
					Set.add(ByteBuffer.wrap(Base64.decode(raw.getString(
						raw.getColumnIndexOrThrow(KVP_KEY)),
						Base64.DEFAULT)));
					}
				while(raw.moveToNext());
				}
			finally
				{
				raw.close();
				}
			}
		finally
			{
			db.close();
			}
		return Set;
		}

	@Override
	public void onCreate(SQLiteDatabase db)
		{
		db.beginTransaction();
		try
			{
			db.execSQL("CREATE TABLE " + KVP_TABLE + " ("
				+ KVP_KEY + " TEXT NOT NULL PRIMARY KEY, "
				+ KVP_VALUE + " TEXT NOT NULL);");
			db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS "
				+ KVP_INDEX + " ON " + KVP_TABLE + " ("
				+ KVP_KEY + ");");
			try { chaff(16, db); }
			catch(Exception ex)
				{
				throw new RuntimeException(ex);
				}
			db.setTransactionSuccessful();
			}
		finally
			{
			db.endTransaction();
			}
    		}

	@Override
	public void onUpgrade(SQLiteDatabase db, int from, int to)
		{
		throw new SQLException("No upgrade path from v" + from + " to v" + to);
		}
	}
