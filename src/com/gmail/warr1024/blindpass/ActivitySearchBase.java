package com.gmail.warr1024.blindpass;

import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.view.View;
import android.widget.TextView;
import java.lang.Runnable;

public abstract class ActivitySearchBase extends ActivityBase
	{
	private Handler tickHandler = null;
	private Runnable tickRun = null;

	public void onSearchStats(long iter, long rate, int found, int lv)
		{
		View ui = findViewById(R.id.search_root);
		if(ui != null)
			{
			View v = ui.findViewById(R.id.search_detail);
			if((v != null) && (v instanceof TextView))
				{
				((TextView)v).setText(getString(R.string.search_detail,
					iter, rate, found, lv));
				}
			}
		}

	public void closeIfNoSearch()
		{
		if(!EngineSearch.getInstance().isRunning())
			finish();
		}

	public void onSearchUpdate()
		{
		long iter = EngineSearch.getInstance().getIteration();

		long start = EngineSearch.getInstance().getStartTime().toMillis(false);
		Time now = new Time();
		now.setToNow();
		long delta = now.toMillis(false) - start;
		long rate = (delta > 0) ? ((iter * 1000) / delta) : 0;

		int found = 0;
		int lv = 0;
		for(SearchResult r : EngineSearch.getInstance().getResults())
			if(r.getExists())
				found++;
			else
				lv++;

		onSearchStats(iter, rate, found, lv);

		closeIfNoSearch();
		}

	public void startUpdater()
		{
		if(tickHandler == null)
			tickHandler = new Handler();
		if(tickRun == null)
			{
			tickRun = new Runnable()
				{
				@Override
				public void run()
					{
					onSearchUpdate();
					tickHandler.postDelayed(tickRun, 500);
					}
				};
			tickRun.run();
			}
		}

	public void stopUpdater()
		{
		if(tickRun != null)
			{
			tickHandler.removeCallbacks(tickRun);
			tickRun = null;
			tickHandler = null;
			}
		}

	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		closeIfNoSearch();
		}

	@Override
	public void onResume()
		{
		super.onResume();
		startUpdater();
		}

	@Override
	protected void onStop()
		{
		stopUpdater();
		super.onStop();
		}
	}
