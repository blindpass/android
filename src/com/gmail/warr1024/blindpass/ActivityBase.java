package com.gmail.warr1024.blindpass;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import java.lang.Runnable;

public abstract class ActivityBase extends Activity
	{
	public void alert(String msg, final Runnable onclose)
		{
		new AlertDialog.Builder(this)
			.setNegativeButton(R.string.alert_dismiss, new DialogInterface.OnClickListener()
				{
				public void onClick(DialogInterface dialog, int id)
					{
					dialog.cancel();
					}
				})
			.setOnCancelListener(new DialogInterface.OnCancelListener()
				{
				public void onCancel(DialogInterface dialog)
					{
					if(onclose != null)
						onclose.run();
					}
				})
			.setMessage(msg)
			.create()
			.show();
		}
	public void alert(String msg)
		{
		alert(msg, null);
		}
	public void alert(int msgId, Runnable onclose)
		{
		alert(AppApplication.getInstance().getString(msgId), onclose);
		}
	public void alert(int msgId)
		{
		alert(msgId, null);
		}
	
	public void startActivity(Class actClass)
		{
		startActivity(new Intent(this, actClass));
		}
	}
