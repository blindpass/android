#!/usr/bin/perl -w

chdir('src/com/gmail/warr1024/blindpass');

my $dh;
opendir($dh, '.') or die($!);
while(my $e = readdir($dh))
	{
	-f $e or next;
	$e =~ m#\.java$# or next;

	my @lines = ( );
	my %imps = ( );
	my $fh;
	open($fh, '<', $e) or next;
	while(<$fh>)
		{
		if(m#^\s*import#)
			{
			$imps{$_} = 1;
			}
		else
			{
			scalar(keys %imps) and
				push @lines, join('', sort { lc($a) cmp lc($b) } keys(%imps));
			%imps = ( );
			push @lines, $_;
			}
		}
	close($fh);

	my $f = '.' . $e . '.new';
	open($fh, '>', $f) or next;
	print $fh join('', @lines);
	close($fh);

	rename($f, $e);
	}
closedir($dh);
