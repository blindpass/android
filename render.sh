#!/bin/sh
PATH="/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin:$PATH"
export PATH

cd svg &&\
find . -type f -name '*.svg' | while read X; do
	echo "########## $X"
	inkscape -z -C -d 90 -e "../res/drawable-ldpi/${X%.svg}.png" -f "$X" &&\
	inkscape -z -C -d 120 -e "../res/drawable-mdpi/${X%.svg}.png" -f "$X" &&\
	inkscape -z -C -d 180 -e "../res/drawable-hdpi/${X%.svg}.png" -f "$X" &&\
	inkscape -z -C -d 240 -e "../res/drawable-xhdpi/${X%.svg}.png" -f "$X" &&\
	inkscape -z -C -d 360 -e "../res/drawable-xxhdpi/${X%.svg}.png" -f "$X" ||\
	exit 1
done
